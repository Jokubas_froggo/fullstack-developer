<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $tasks = request()->user()->tasks;

        return response()->json([
            'tasks' => $tasks,
        ], 200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'           => 'required|max:255',
            'description'    => 'required',
            'date'           => 'required',
            'location'       => 'required',
            'time_from'      => 'required',
            'time_to'        => 'required',
            'notify_message' => 'required',
            'notify_type'    => 'required',
            'color_code'     => 'max:255',
        ]);

        $task = Task::create([
            'name'           => request('name'),
            'description'    => request('description'),
            'date' 	         => request('date'),
            'location' 	     => request('location'),
            'time_from'      => request('time_from'),
            'time_to' 	     => request('time_to'),
            'notify_message' => request('notify_message'),
            'notify_type' 	 => request('notify_type'),
            'color_code'     => request('color_code'),
            'user_id'        => Auth::user()->id
        ]);

        return response()->json([
            'task'    => $task,
            'message' => 'Success'
        ], 200);
    }

    public function update(Request $request, Task $task)
    {
        $this->validate($request, [
            'name'        => 'required|max:255',
            'description' => 'required',
            'date'           => 'required',
            'location'       => 'required',
            'time_from'      => 'required',
            'time_to'        => 'required',
            'notify_message' => 'required',
            'notify_type'    => 'required',
            'color_code'     => 'max:255',
        ]);

        $task->name           = request('name');
        $task->description    = request('description');
        $task->date           = request('date');
        $task->location       = request('location');
        $task->time_from      = request('time_from');
        $task->time_to        = request('time_to');
        $task->notify_message = request('notify_message');
        $task->notify_type    = request('notify_type');
        $task->color_code     = request('color_code');
        $task->save();

        return response()->json([
            'message' => 'Task updated successfully!'
        ], 200);
    }


    public function destroy(Task $task)
    {
        $task->delete();

        return response()->json([
            'message' => 'Task deleted successfully!'
        ], 200);
    }
}
