<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
 		'user_id', 
 		'name', 
 		'description', 
 		'date', 
 		'time_from', 
 		'time_to', 
 		'location', 
 		'notify_message', 
 		'notify_type',
 		'color_code'
 	];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
