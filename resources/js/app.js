/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import './bootstrap';
import 'angular';
import 'angular-moment-picker';

var app = angular.module('eventsCRUD', ['moment-picker']
    , ['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.headers.post['X-CSRF-TOKEN'] = $('meta[name=csrf-token]').attr('content');
    }]);
app.config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
}]);
app.controller('TaskController', ['$scope', '$http', function ($scope, $http) {
	// Show hide delete ico
	$scope.open = false;

    $scope.tasks = [];

    // List tasks
    $scope.loadTasks = function () {
        $http.get('http://localhost/laravelprojects/flevents/public/task')
            .then(function success(e) {
                $scope.tasks = e.data.tasks;               
               
            });
    };
    $scope.loadTasks();

    $scope.errors = [];

    $scope.task = {
        name: '',
        description: '',
        date: '',
        location: '',
        time_from: '',
        time_to: '',
        notify_message: '',
        notify_type: '',
        color_code: '',
    };
    $scope.initTask = function () {
        $scope.resetForm();
        $("#add_new_task").modal('show');
    };

    // Add new Task
    $scope.addTask = function () {
        $http.post('http://localhost/laravelprojects/flevents/public/task', {
            name: $scope.task.name,
            description: $scope.task.description,
            date: $scope.task.date,
            location: $scope.task.location,
            time_from: $scope.task.time_from,
            time_to: $scope.task.time_to,
            notify_message: $scope.task.notify_message,
            notify_type: $scope.task.notify_type,
            color_code: $scope.task.color_code,
        }).then(function success(e) {
            $scope.resetForm();
            $scope.tasks.push(e.data.task);
            $("#add_new_task").modal('hide');

        }, function error(error) {
            $scope.recordErrors(error);
        });
    };

    $scope.recordErrors = function (error) {
        $scope.errors = [];
        if (error.data.errors.name) {
            $scope.errors.push(error.data.errors.name[0]);
        }

        if (error.data.errors.description) {
            $scope.errors.push(error.data.errors.description[0]);
        }

        if (error.data.errors.date) {
            $scope.errors.push(error.data.errors.date[0]);
        }

        if (error.data.errors.location) {
            $scope.errors.push(error.data.errors.location[0]);
        }

        if (error.data.errors.time_from) {
            $scope.errors.push(error.data.errors.time_from[0]);
        }

        if (error.data.errors.time_to) {
            $scope.errors.push(error.data.errors.time_to[0]);
        }

        if (error.data.errors.notify_message) {
            $scope.errors.push(error.data.errors.notify_message[0]);
        }

        if (error.data.errors.notify_type) {
            $scope.errors.push(error.data.errors.notify_type[0]);
        }


        if (error.data.errors.color_code) {
            $scope.errors.push(error.data.errors.color_code[0]);
        }

    };

    $scope.resetForm = function () {
        $scope.task.name = '';
        $scope.task.description = '';
        $scope.task.date = '';
        $scope.task.location = '';
        $scope.task.time_from = '';
        $scope.task.time_to = '';
        $scope.task.notify_message = '';
        $scope.task.notify_type = '';
        $scope.task.color_code = '';
        $scope.errors = [];
    };

    $scope.edit_task = {};
    // initialize update action
    $scope.initEdit = function (index) {
        $scope.errors = [];
        $scope.edit_task = $scope.tasks[index];
        $scope.edit_task.date = new Date($scope.edit_task.date);
        $("#edit_task").modal('show');
    };

    // update the given task
    $scope.updateTask = function () {
        $http.patch('http://localhost/laravelprojects/flevents/public/task/' + $scope.edit_task.id, {
            name: $scope.edit_task.name,
            description: $scope.edit_task.description,
            date: $scope.edit_task.date,
            location: $scope.edit_task.location,
            time_from: $scope.edit_task.time_from,
            time_to: $scope.edit_task.time_to,
            notify_message: $scope.edit_task.notify_message,
            notify_type: $scope.edit_task.notify_type,
            color_code: $scope.edit_task.color_code
        }).then(function success(e) {
            $scope.errors = [];
            $("#edit_task").modal('hide');
        }, function error(error) {
            $scope.recordErrors(error);
        });
    };    

    // delete the given task
    $scope.deleteTask = function (index) {

        var conf = confirm("Do you really want to delete this task?");

        if (conf === true) {
            $http.delete('http://localhost/laravelprojects/flevents/public/task/' + $scope.tasks[index].id)
                .then(function success(e) {
                    $scope.tasks.splice(index, 1);
                });
        }
    };

}]);


//window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/*const app = new Vue({
    el: '#app',
});*/
