@extends('layouts.app')

@section('content')
<div class="container" ng-controller="TaskController">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Home
                    <img src="{{ asset('images/plus-ico.png') }}" class="img-fluid float-right d-block" alt="" ng-click="initTask()">                    
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="events-holder" ng-if="tasks.length > 0">                        
                        <div ng-repeat="(index, task) in tasks" style="border-left: 9px solid @{{ task.color_code }}" class="row event">
                            <div class="col-md-12">
                                <div class="content">                                    
                                    <div class="title-holder" ng-click="initEdit(index)">
                                        <h3>@{{ task.name }}</h3>
                                    </div>
                                    <div class="time-place-holder" ng-click="initEdit(index)">                               
                                        <div class="time">
                                            <span>@{{ task.time_from }}</span>-<span>@{{ task.time_to }}</span>
                                        </div>
                                        <div class="place">@{{ task.location }}</div>
                                    </div>
                                    <div class="delete-holder" ng-mouseover="open = true" ng-mouseleave="open = false">                                       
                                        <img ng-hide="open"  src="{{ asset('images/check-ico.png') }}" class="img-fluid check-icon float-right d-block" alt="" ng-click="deleteTask(index)"> 
                                        <img ng-show="open" src="{{ asset('images/delete-ico.png') }}" class="img-fluid delet-icon float-right" alt="" ng-click="deleteTask(index)">
                                    </div>
                                </div>                                
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="add_new_task">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Task</h4>
                </div>
                <div class="modal-body">

                    <div class="alert alert-danger" ng-if="errors.length > 0">
                        <ul>
                            <li ng-repeat="error in errors">@{{ error }}</li>
                        </ul>
                    </div>
                    <div class="form-group">
                        <label for="name">Name @{{task.time_from}}</label>
                        <input type="text" name="name" class="form-control" ng-model="task.name">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" rows="3" class="form-control"
                                  ng-model="task.description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="date">Date</label>
                        <input type="date" name="date" class="form-control" ng-model="task.date">
                    </div>
                    <div class="form-row">
                        <div class="col-md-6">
                            
                            <div class="form-group" moment-picker="task.time_from" format="HH:mm">
                                <label for="time_from">From</label>                        
                                <input class="form-control"
                                        name="time_from"
                                        placeholder="Select a time"
                                        ng-model="task.time_from"
                                        ng-model-options="{ updateOn: 'blur' }">
                            </div>
                        </div>
                        <div class="col-md-6">
                            
                            <div class="form-group" moment-picker="task.time_to" format="HH:mm">
                                <label for="time_to">To</label>                       
                                <input class="form-control"
                                        name="time_to"
                                        placeholder="Select a time"
                                        ng-model="task.time_to"
                                        ng-model-options="{ updateOn: 'blur' }">
                            </div>          
                        </div>
                    </div>
                              
                    <div class="form-group">
                        <label for="location">Location</label>
                        <input type="text" name="location" class="form-control" ng-model="task.location">
                    </div>
                    <div class="form-group">
                        <label for="notify_message">Notify</label>
                        <input type="text" name="notify_message" class="form-control" ng-model="task.notify_message">
                    </div>
                    <div class="form-group">
                        <label for="notify_type" >Type</label>
                        <select class="form-control" name="notify_type" ng-model="task.notify_type">
                            <option value="email" selected>Email</option>
                            <option value="msg">Message</option>                          
                        </select>                        
                    </div>
                    <div class="form-group color-picker">
                        <label for="color_code">Label</label>
                        <input type="text" name="color_code" class="form-control" ng-model="task.color_code">
                        <input type="color" name="favcolor" class="form-control color-sample" value="#ff0000" ng-model="task.favcolor">
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" ng-click="addTask()">Submit</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="edit_task">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Task</h4>
                </div>
                <div class="modal-body">

                    <div class="alert alert-danger" ng-if="errors.length > 0">
                        <ul>
                            <li ng-repeat="error in errors">@{{ error }}</li>
                        </ul>
                    </div>

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" ng-model="edit_task.name">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" rows="3" class="form-control"
                                  ng-model="edit_task.description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="date">Date</label>
                        <input type="date" name="date" class="form-control" ng-model="edit_task.date">
                    </div>
                    
                    <div class="form-group" moment-picker="edit_task.time_from" format="HH:mm">
                        <label for="time_from">From</label>                        
                        <input class="form-control"
                                name="time_from"
                                placeholder="Select a time"
                                ng-model="edit_task.time_from"
                                ng-model-options="{ updateOn: 'blur' }">
                    </div>
                    <div class="form-group" moment-picker="edit_task.time_to" format="HH:mm">
                        <label for="time_to">To</label>                       
                        <input class="form-control"
                                name="time_to"
                                placeholder="Select a time"
                                ng-model="edit_task.time_to"
                                ng-model-options="{ updateOn: 'blur' }">
                    </div>                    
                    <div class="form-group">
                        <label for="location">Location</label>
                        <input type="text" name="location" class="form-control" ng-model="edit_task.location">
                    </div>
                    <div class="form-group">
                        <label for="notify_message">Name</label>
                        <input type="text" name="notify_message" class="form-control" ng-model="edit_task.notify_message">
                    </div>
                    <div class="form-group">
                        <label for="notify_type" >Type</label>
                        <select class="form-control" name="notify_type" ng-model="edit_task.notify_type">
                            <option value="email">Email</option>
                            <option value="msg">Message</option>                          
                        </select>                        
                    </div>
                    <div class="form-group color-picker">
                        <label for="color_code">Label</label>
                        <input type="text" name="color_code" class="form-control" ng-model="edit_task.color_code">
                        <input type="color" name="favcolor" class="form-control color-sample" value="#ff0000" ng-model="edit_task.color_code">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" ng-click="updateTask()">Submit</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
@endsection
