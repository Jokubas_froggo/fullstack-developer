@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <ul>
                <li>
                    jMJnJDSXqs@gmail.com
                </li>
                <li>
                    secret
                </li>
            </ul>
            <div class="logo-holder">
                <img src="{{ asset('images/home-logo.png') }}" class="img-fluid mx-auto d-block" alt="">
            </div>
            <div class="card d-flex justify-content-center">
                <!--<div class="card-header">{{ __('Login') }}</div>-->

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group form-row">

                            <div class="col-md-6">
                            <label for="email" class="col-form-label text-md-right">{{ __('USERNAME') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group form-row">

                            <div class="col-md-6">
                            <label for="password" class="col-form-label text-md-right">{{ __('PASSWORD') }}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label for="" class="col-form-label text-md-right">&nbsp;</label>
                                <div>                                    
                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link float-right" href="{{ route('password.request') }}">
                                            {{ __('Forgot Password') }}
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!--<div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>-->

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-custom">
                                    {{ __('Sign In') }}
                                </button>                              
                            </div>                            
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12 text-center">
                                <div class="line-holder">                                    
                                    <span class="account-line">Don't have an account?</span><a href="{{ route('register') }}" class="signup">Sign up</a>                         
                                </div>
                            </div>                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
