@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="upload-image">
                <img src="{{ asset('images/personal-picture.jpg') }}" class="img-fluid mx-auto d-block" alt="">
            </div>
            <div class="card">
                <div class="card-header">{{ __('Sign up') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group form-row">

                            <div class="col-md-6">
                            <label for="name" class="col-form-label text-md-right">{{ __('FULL NAME') }}</label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group form-row">

                            <div class="col-md-6">
                            <label for="email" class="col-form-label text-md-right">{{ __('E-Mail') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group form-row">

                            <div class="col-md-6">
                            <label for="password" class="col-form-label text-md-right">{{ __('PASSWORD') }}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group form-row">

                            <div class="col-md-6">
                            <label for="password-confirm" class="col-form-label text-md-right">{{ __('CONFIRM PASSWORD') }}</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                        
                        <div class="form-group form-row">

                            <div class="col-md-6">
                            <label for="birthday" class="col-form-label text-md-right">{{ __('BIRTHDAY') }}</label>
                                <input id="birthday" type="text" class="form-control" name="birth_date" required >
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-custom">
                                    {{ __('Create') }}
                                </button>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12 text-center">
                                <div class="line-holder">                                    
                                    <span class="account-line">Already Have an account?</span><a href="{{ route('login') }}" class="signup">Sign In</a>                         
                                </div>
                            </div>                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
